class PF2eCritFumbleDeck {
	constructor() {
		this.critTable;
		this.fumbleTable;
		this.diceSoNice = false;
	}

	init() {
		this.diceSoNice = (game.modules.get('dice-so-nice') && game.modules.get('dice-so-nice').active);
		this.loadRollTables();
		this.applyHook();
	}

	async loadRollTables() {
		const rollableTables = game.packs.get('pf2e.rollable-tables');
		this.critTable = new RollTable(await rollableTables.getEntry('FTEpsIWWVrDj0jNG'));
		this.fumbleTable = new RollTable(await rollableTables.getEntry('WzMGWMIrrPvSp75D'));
	}

	applyHook() {
		const hooksOn = this.diceSoNice ? 'diceSoNiceRollComplete' : 'createChatMessage';
		Hooks.on(hooksOn, this.handleRoll.bind(this))
	}

	handleRoll(chatMessage) {
		chatMessage = this.diceSoNice ? game.messages.get(chatMessage) : chatMessage;
		if (chatMessage.isAuthor && chatMessage.isRoll && chatMessage.isContentVisible) {
			if (chatMessage.data.flavor.startsWith('<b>Strike:') || chatMessage.data.flavor.includes('Attack Roll')) {
				const die = chatMessage.roll.dice[0];
				if (die.faces === 20) {
					if (die.total === 20) {
						this.drawCard(this.critTable, chatMessage);
					} else if (die.total === 1) {
						this.drawCard(this.fumbleTable, chatMessage);	
					}
				}
			}
		}
	}

	drawCard(table, chatMessage) {
		if (!this.diceSoNice) mergeObject(chatMessage.data, { ['-=sound']: null });
		table.draw();
	}
}

PF2eCritFumbleDeckSingleton = new PF2eCritFumbleDeck();

Hooks.once('ready', PF2eCritFumbleDeckSingleton.init.bind(PF2eCritFumbleDeckSingleton));
